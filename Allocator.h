// ------------------------------
// projects/allocator/Allocator.h
// Copyright (C) 2013
// Glenn P. Downing
// ------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert> // assert
#include <new> // new
#include <stdexcept> // invalid_argument


int& view(char& ptr){
  return *reinterpret_cast<int*>(&ptr);
}

// ---------
// Allocator
// ---------

template <typename T, int N>
class Allocator {
    public:
        // --------
        // typedefs
        // --------

        typedef T value_type;

        typedef int size_type;
        typedef std::ptrdiff_t difference_type;

        typedef value_type* pointer;
        typedef const value_type* const_pointer;

        typedef value_type& reference;
        typedef const value_type& const_reference;

    public:
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const Allocator&, const Allocator&) {
            return true;}

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const Allocator& lhs, const Allocator& rhs) {
            return !(lhs == rhs);}

    private:
        // ----
        // data
        // ----

        char a[N];
        int len;

        // -----
        // valid
        // -----

        /**
* O(1) in spacE
* O(n) in time
* Takes in no arguements. Returns bool
* Tests validity of the Allocator
* Returns true if Allocator data is not corrupted
*/
        bool valid () const {
            // <your code>
            int count = 8;
            int ptr = 0;
            int* beg = (int*)(&a[0]);
            ptr += abs(*beg)+4;
            int* end = (int*)(&a[ptr]);

            while(count < len){
              //sentinel check
              if(*beg < -(len-4) || *beg > (len-4) ||
                 *end < -(len-4) || *end > (len-4))
                return false;

              //inconsistent sentinel check
              if(*beg != *end)
                return false;

              //check if absolute value is larger than allocator size
              int absval = abs(*beg);
              if(absval > (len-8) || count > (len-8))
                return false;

              count += absval;

              //get next set of sentinels
              if(count < (len-8) && ptr != (len-4)){
                count+=8;
                ptr+=4;
                beg = (int*)(&a[ptr]); 
                ptr = abs(*beg)+ptr+4;
                end = (int*)(&a[ptr]); 
              }
            }

            if(count != len) 
              return false;

            return true;}

    public:
        // ------------
        // constructors
        // ------------

        /**
* O(1) in space
* O(1) in time
* Default constructor for the Allocator
* Saves allocator size, initializes first two sentinels,
*  and checks validity of the structure
*/
        Allocator () {
            len = N;
            view(a[0]) = len-8;
            view(a[len-4]) = len-8;
            assert(valid());}

        // Default copy, destructor, and copy assignment
        // Allocator (const Allocator<T>&);
        // ~Allocator ();
        // Allocator& operator = (const Allocator&);

        // --------
        // allocate
        // --------

        /**
* O(1) in space
* O(n) in time
* Takes in 1 arguement of type size_type
* Checks open memory in the Allocator. If found, sets sentinels to
*  taken space and returns pointer to newly allocated space.
* after allocation there must be enough space left for a valid block
* the smallest allowable block is sizeof(T) + (2 * sizeof(int))
* choose the first block that fits
*/
        pointer allocate (size_type n) {
            // <your code>
            pointer res;
            int count = 8; 
            int amountreq = sizeof(T) * n;

            //pre-check for bad alloc on input
            if(amountreq+8 > len  || amountreq < 1){
              throw std::bad_alloc();}

            int ptr = 0;
            bool search = true;
            while(search){
              int* sent = (int*)(&a[ptr]);
              int block = abs(*sent); 

              if(block >= (amountreq + 8)){

                //if min requirement not met
                if((block - amountreq) < (signed)(sizeof(T) + (2 * sizeof(int)))){
                  view(a[ptr]) = -block;
                  view(a[ptr+block+4]) = view(a[ptr]);
                  res =(pointer) (&a[ptr+4]);
                  break;
                }
                else{
                  //set new sentinels
                  view(a[ptr]) = -amountreq;
                  view(a[ptr+amountreq+4]) = -amountreq;

                  int shortened = block - (amountreq+8);
                  view(a[ptr+amountreq+8]) = shortened;
                  view(a[ptr+block+4]) = shortened;
                  res = (pointer) (&a[ptr+4]);
                  break;
                }
              }
              count +=block;
              if(count >= len){
                throw std::bad_alloc();}
              ptr += block + 8; 
            }
            assert(valid());
            return res;}

        // ---------
        // construct
        // ---------

        /**
* O(1) in space
* O(1) in time
* Takes a pointer and const_reference as arguements
* constructs a type T at pointer p given reference v.
* Does not return.
*/
        void construct (pointer p, const_reference v) {
            new (p) T(v); // uncomment!
            assert(valid());}

        // ----------
        // deallocate
        // ----------

        /**
* O(1) in space
* O(1) in time
* Deallocate frees room oh memory by changing signs of the sentinels.
* Additionally coallesces free room.
* Takes in pointer of memory being deallocated and size as arguements
* after deallocation adjacent free blocks must be coalesced
*/
        void deallocate (pointer p, size_type = 0) {
            // <your code>
            char* head = (char*)p;
            int size =abs( *(head-4));
            //coalesce all
            if((head-4) != &a[0] && (head+size) != &a[len-4] &&
                   *(head-8) > 0 && *(head+size+4) > 0){
              int* backtail= (int*)(head-8);
              int backsize = *backtail;
              backtail = (int*)(head+size+4);
              int forwardsize = *backtail; 
 
              //resetting sentinels to 0
              view( *(head-4) ) = 0;
              view( *(head-8) ) = 0;
              view( *(head+size)) = 0;
              view( *(head+size+4)) = 0;

              //setting new sent
              view( *(head-12-backsize)) = backsize + size + forwardsize+ 16;
              view( *(head+size+8+forwardsize)) = backsize+size+forwardsize+16;
            }
            else if((head-4) != &a[0] &&  *(head-8)>0 ){
              //coalesce backwards
              int* backtail= (int*)(head-8);
              int backsize = *backtail;

              view( *(head-4) ) = 0;
              view( *(head-8) ) = 0;

              view( *(head-12-backsize)) = backsize + size + 8;
              view( *(head+size) ) = backsize+size+8;

            }
            else if( ((head+size) != &a[len-4] ) && ( *(head+size+4)>0 ) ){
              //coalesce forward
              int* forwardhead = (int*)(head+size+4);
              int forwardsize = *forwardhead; 

              view( *(head+size) ) = 0;
              view( *(head+size+4) ) = 0;
              
              view( *(head-4) ) = size + 8 + forwardsize;
              view( *(head+size+8+forwardsize) ) = size + 8 + forwardsize;
            }
            else{
              //default dealloc
              view(*(head-4)) = -view(*(head-4));
              view(*(head+size)) = view(*(head-4));
            }
            assert(valid());}

        // -------
        // destroy
        // -------

        /**
* O(1) in space
* O(1) in time
* destroys the opject at pointer P using destructor of type T
*/
        void destroy (pointer p) {
            p->~T(); // uncomment!
            assert(valid());}};

#endif // Allocator_h
